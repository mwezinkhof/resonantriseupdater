/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package novaz.worker;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by kaaz on 22-4-2015.
 */
public class SimpleDownloader {

	public static String getContents(String url) {
		StringBuilder response = new StringBuilder();
		try {
			URL targetUrl = new URL(url);
			HttpURLConnection request = (HttpURLConnection) targetUrl.openConnection();
			InputStream in = null;
			in = request.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String inputLine;
			while ((inputLine = br.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			request.disconnect();
		} catch (Exception e) {
			System.out.println(e);
		}

		return response.toString();
	}

}
