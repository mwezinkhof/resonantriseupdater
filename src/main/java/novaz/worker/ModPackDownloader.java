/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package novaz.worker;

import novaz.Config;
import novaz.model.McLibrary;
import novaz.model.McMod;
import novaz.tools.FileUtil;
import novaz.tools.Log;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by kaaz on 22-4-2015.
 */
public class ModPackDownloader {
	private String packName, version, xml;
	private ArrayList<String> requirements;
	private ArrayList<McMod> modlist;
	private ArrayList<McLibrary> libraries;
	private String launchJar;

	public ModPackDownloader(String pack, String version) {
		this.packName = pack;
		this.version = version;
		modlist = new ArrayList<>();
		modlist.add(new McMod("SERVER", "https://s3.amazonaws.com/Minecraft.Download/versions/1.7.10/minecraft_server.1.7.10.jar", "minecraft_server.1.7.10.jar", "forge"));
		libraries = new ArrayList<>();
		requirements = new ArrayList<>();

	}

	public void addRequirement(String modName) {
		requirements.add(modName);
	}

	public boolean updateServer() {
		if (!backupOldWorld()) {
			return false;
		}
		try {
			String[] directoriesToClean = {"asm", "config", "libraries", "mods", "scripts"};
			for (String dirToClean : directoriesToClean) {
				File tmp = new File(Config.SERVER_DIR + dirToClean);
				if (tmp.exists()) {
					FileUtil.removeRecursive(tmp.toPath());
				}
			}
			this.xml = SimpleDownloader.getContents(Config.BASE_URL + "packs/" + getSafeName() + "/versions/" + version + "/Configs.xml");
			downloadEntries();
			downloadMods();
			FileUtils.writeStringToFile(new File(Config.SERVER_DIR + Config.MC_LAUNCH_SCRIPT), String.format("java -Xmx2G -XX:MaxPermSize=256M -jar %s nogui", launchJar));
			FileUtils.writeStringToFile(new File(Config.SERVER_DIR + "eula.txt"), "eula=true");
			return true;
		} catch (Exception e) {
			Log.fatal(e.getMessage());
			Log.fatal(e.getStackTrace());
			return false;
		}

	}

	private boolean backupOldWorld() {
		String world = Config.getMinecraftServerProperty("level-name", "world");
		File worldFolder = new File(Config.SERVER_DIR + world);
		if (worldFolder.exists()) {
			Log.info(String.format("Found world backuping '%s'", worldFolder));
			try {
				FileUtil.copyFolder(worldFolder, new File(Config.SERVER_DIR + "backup/" + Config.INSTALLED_PACK_VERSION));
			} catch (Exception e) {
				Log.fatal("Couldn't copy the old world");
				Log.fatal(e.getStackTrace());
				return false;
			}
		}
		return true;
	}

	public String getSafeName() {
		return this.packName.replaceAll("[^A-Za-z0-9]", "");
	}

	public void downloadEntries() throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(xml));
		Document document = builder.parse(is);
		document.getDocumentElement().normalize();
		NodeList nodeList = document.getElementsByTagName("mod");
		Node node;
		int count = 0;
		for (int i = 0; i < nodeList.getLength(); i++) {
			node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				String name = element.getAttribute("name");
				String depends = element.getAttribute("depends");
				String type = element.getAttribute("type");
				if (element.getAttribute("server").equalsIgnoreCase("no")) {
					continue;
				}
				boolean optional = false;
				if (element.getAttribute("optional").equalsIgnoreCase("yes")) {
					optional = true;
				}
				boolean serverOptional = optional;
				if (element.getAttribute("serveroptional").equalsIgnoreCase("yes")) {
					serverOptional = true;
				} else if (element.getAttribute("serveroptional").equalsIgnoreCase("no")) {
					serverOptional = false;
				}
				if (!serverOptional) {
					requirements.add(name);
				}
				if (requirements.contains(name) || type.equals("forge")) {
					if (depends.contains(",")) {
						for (String dependency : depends.split(",")) {
							if (!requirements.contains(dependency)) {
								requirements.add(dependency);
							}
						}
					} else if (depends.length() > 0) {
						if (!requirements.contains(depends)) {
							requirements.add(depends);
							if (depends.equals("CoFH Framework")) {
								requirements.add("CoFH Core");
								requirements.add("CoFH Lib");
							}
						}
					}
				}

			}
		}
		for (int i = 0; i < nodeList.getLength(); i++) {
			node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				String name = element.getAttribute("name");
				String version = element.getAttribute("version");
				String url = element.getAttribute("url");
				String file = element.getAttribute("file");
				String type = element.getAttribute("type");
				if (requirements.contains(name) || type.equals("forge")) {
					modlist.add(new McMod(name, url, file, type));
					Log.debug(String.format("name=%s, version=%s, url=%s,file=%s,type=%s", name, version, url, file, type));
					count++;
				}
			}
		}
		NodeList libList = document.getElementsByTagName("library");
		ArrayList<String> doubleFiles = new ArrayList<>();
		for (int i = 0; i < libList.getLength(); i++) {
			node = libList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				String url = element.getAttribute("url");
				String file = element.getAttribute("file");
				String server = element.getAttribute("server");

				if (server.length() > 0) {
					if (!doubleFiles.contains(server)) {
						doubleFiles.add(server);
						libraries.add(new McLibrary(url, file, server));
					}
				}
			}
		}
		Log.debug(String.format("downloading %s entries.", count));
	}

	public void downloadMods() throws IOException {
		File f = new File(Config.SERVER_DIR + "mods");
		f.mkdirs();
		File f2 = new File(Config.SERVER_DIR + "libraries");
		f2.mkdirs();

		for (McLibrary l : libraries) {
			File tmp = new File(Config.SERVER_DIR + "libraries/" + l.serverLocation.substring(0, l.serverLocation.lastIndexOf("/")));
			tmp.mkdirs();
			downloadFile(Config.SERVER_DIR + "libraries/" + l.serverLocation, Config.BASE_URL + l.url);
			Log.debug("downloading: " + l.file);
		}

		for (McMod m : modlist) {
			if (m.type.equals("dependency")) {
				downloadFile(Config.SERVER_DIR + "mods/1.7.10/" + m.filename, Config.BASE_URL + m.url);
				Log.debug("downloading: " + m.filename);
			} else if (m.type.equals("mods")) {
				downloadFile(Config.SERVER_DIR + "mods/" + m.filename.toUpperCase().replace(".JAR", ".jar"), Config.BASE_URL + m.url);
				Log.debug("downloading: " + m.filename);
			} else if (m.type.equals("forge")) {
				if (m.name.equals("SERVER")) {
					downloadFile(Config.SERVER_DIR + m.filename, m.url);
				} else {
					downloadFile(Config.SERVER_DIR + m.filename, Config.BASE_URL + m.url);
					launchJar = m.filename;
				}
				Log.debug("downloading: " + m.filename);
			} else if (m.type.equals("extract")) {
				Log.debug("Unzipping: " + m.filename);
				unZipIt(Config.BASE_URL + m.url, Config.SERVER_DIR);
			} else {
				Log.debug(String.format("Don't know what to do with type %s; file=%s", m.type, m.filename));
			}
		}
	}

	public void downloadFile(final String filename, final String urlString)
			throws MalformedURLException, IOException {
		BufferedInputStream in = null;
		FileOutputStream fout = null;
		try {
			File f = new File(filename);
			f.getParentFile().mkdirs();
			in = new BufferedInputStream(new URL(urlString).openStream());
			fout = new FileOutputStream(filename);

			final byte data[] = new byte[1024];
			int count;
			while ((count = in.read(data, 0, 1024)) != -1) {
				fout.write(data, 0, count);
			}
		} finally {
			if (in != null) {
				in.close();
			}
			if (fout != null) {
				fout.close();
			}
		}
	}

	public void unZipIt(String zipLocation, String outputFolder) throws IOException {


		byte[] buffer = new byte[1024];

		try {

			//create output directory is not exists
			File folder = new File(outputFolder);
			if (!folder.exists()) {
				folder.mkdir();
			}
			URL url = new URL(zipLocation);
			ZipInputStream zis = new ZipInputStream(url.openStream());

			//get the zipped file list entry
			ZipEntry ze = zis.getNextEntry();

			while (ze != null) {

				String fileName = ze.getName();
				File newFile = new File(outputFolder + File.separator + fileName);
				if (ze.isDirectory()) {
					newFile.mkdirs();
				} else {
					new File(newFile.getParent()).mkdirs();
					FileOutputStream fos = new FileOutputStream(newFile);

					int len;
					while ((len = zis.read(buffer)) > 0) {
						fos.write(buffer, 0, len);
					}

					fos.close();
				}

				ze = zis.getNextEntry();
			}

			zis.closeEntry();
			zis.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
