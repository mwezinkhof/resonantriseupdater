/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package novaz.worker;

import novaz.tools.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by kaaz on 24-4-2015.
 */
public class ScreenManager {
	String screenName;

	public ScreenManager(String screenName) {
		this.screenName = screenName;
		prepareScreen();
	}

	private void prepareScreen() {
		if (!screenExists()) {
			makeScreen();
		}
	}

	private boolean screenExists() {
		try {
			Process p = Runtime.getRuntime().exec("screen -list");
			InputStreamReader isr = new InputStreamReader(p.getInputStream());
			p.waitFor();
			BufferedReader br = new BufferedReader(isr);
			String line = "";
			Log.debug("screen exists?");
			while ((line = br.readLine()) != null) {
				if (line.contains("." + screenName)) {
					Log.debug("Yes");
					return true;
				}
			}
		} catch (Exception e) {
			Log.fatal(e.getStackTrace());
		}
		Log.debug("No");
		return false;
	}

	public void makeScreen() {
		try {
			Process p = Runtime.getRuntime().exec(new String[]{"bash", "-c", "screen -d -m -S " + screenName});
			p.waitFor();
		} catch (Exception e) {
			Log.fatal(e.getStackTrace());//e.printStackTrace();
		}
	}

	public void commandOnScreen(String command) {
		try {
			String[] cmd = {"screen", "-p", "0",
					"-S", screenName, "-X", "eval", "stuff \""+command+"\"\\015"};
			Process p = Runtime.getRuntime().exec(cmd);
			p.waitFor();
		} catch (Exception e) {
			Log.fatal(e.getStackTrace());
		}
	}
}
