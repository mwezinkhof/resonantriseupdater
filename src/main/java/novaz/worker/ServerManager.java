/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package novaz.worker;

import novaz.Config;
import novaz.tools.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Created by kaaz on 24-4-2015.
 */
public class ServerManager {
	String host;
	int port;
	ScreenManager screenManager;

	public ServerManager(String host, String port) {
		this.host = host;
		screenManager = new ScreenManager(Config.SCREEN_NAME);
		try {
			this.port = Integer.parseInt(port);
		} catch (Exception ignored) {
			this.port = 25565;//default
		}
	}

	public void start() {
		if (!isOnline()) {
			screenManager.commandOnScreen("cd ~/" + Config.SERVER_DIR);
			screenManager.commandOnScreen("sh " + Config.MC_LAUNCH_SCRIPT);

		}
	}

	public void stop() {
		if (isOnline()) {
			screenManager.commandOnScreen("save-all");
			screenManager.commandOnScreen("stop");
		}
	}

	public boolean isOnline() {
		int failCount = 0;


		Socket socket = null;
		DataInputStream datainputstream = null;
		DataOutputStream dataoutputstream = null;

		try {
			socket = new Socket();

			socket.setSoTimeout(3000);
			socket.setTcpNoDelay(true);
			socket.setTrafficClass(18);

			socket.connect(new InetSocketAddress(host, port), 3000);

			datainputstream = new DataInputStream(socket.getInputStream());
			dataoutputstream = new DataOutputStream(socket.getOutputStream());

			dataoutputstream.write(254);

			if (datainputstream.read() != 255) throw new IOException("Bad message");

			short strLength = datainputstream.readShort();
			if (strLength < 0 || strLength > 64) throw new IOException("invalid string length");

			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < strLength; i++) stringBuilder.append(datainputstream.readChar());

			String data = stringBuilder.toString();
			String dataParts[] = data.split("\u00a7");

			String motd = dataParts[0];

			int playerCount = -1;
			int maxPlayerCount = -1;

			try {
				playerCount = Integer.parseInt(dataParts[1]);
				maxPlayerCount = Integer.parseInt(dataParts[2]);
				Log.debug(String.format("players: %02d/%02d", playerCount, maxPlayerCount));
				return true;
			} catch (Exception ignored) {
			}
		} catch (Exception ignored) {
		} finally {
			try {
				if (datainputstream != null) datainputstream.close();
			} catch (Throwable ignored) {
			}
			try {
				if (dataoutputstream != null) dataoutputstream.close();
			} catch (Throwable ignored) {
			}
			try {
				if (socket != null) socket.close();
			} catch (Throwable ignored) {
			}

		}
		return false;
	}

	public void sendMessage(String message) {
		screenManager.commandOnScreen("say " + message);
	}
}
