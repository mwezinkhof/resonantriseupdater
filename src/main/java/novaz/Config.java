/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package novaz;

import novaz.tools.Log;
import novaz.tools.LogLevel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

/**
 * Created by kaaz on 22-4-2015.
 */
public class Config {
	public static String BASE_URL = "http://download.nodecdn.net/containers/atl/";
	public static String PACK_LIST = "launcher/json/packs.json";
	public static String SERVER_DIR = "server/";
	public static String PACK_NAME = "Resonant Rise";
	public static String PROPERTY_FILE = "PackUpdater.cfg";
	public static String MC_LAUNCH_SCRIPT= "start.sh";
	public static String INSTALLED_PACK_VERSION = "";
	public static LogLevel loglevel = LogLevel.INFO;
	private static boolean initialized = false;
	private static Properties propObj;
	public static String SCREEN_NAME = "MC_SERVER";

	public static void loadProperties() {
		if (!initialized) {
			propObj = getProps(PROPERTY_FILE);
			INSTALLED_PACK_VERSION = propObj.getProperty("installed_pack_version", "VERSION_0");
			PACK_NAME = propObj.getProperty("pack_name", PACK_NAME);
			initialized=true;
		}
	}

	public static String getMinecraftServerProperty(String propertyName, String defaultValue) {
		Properties p = getProps("server.properties");
		if (p != null) {
			return p.getProperty(propertyName, defaultValue);
		}
		return defaultValue;

	}

	public static void saveProperties() {
		Properties p = getProps(PROPERTY_FILE);
		p.put("installed_pack_version", INSTALLED_PACK_VERSION);
		p.put("pack_name", PACK_NAME);

		try {
			p.store(new FileOutputStream(SERVER_DIR + PROPERTY_FILE), null);
		} catch (Exception e) {
			Log.fatal("couldn't save property file");
			Log.fatal(e.getStackTrace());
		}
	}

	private static Properties getProps(String filename) {
		try {
			File f = new File(SERVER_DIR + filename);
			if (!f.exists()) {
				f.getParentFile().mkdirs();
				f.createNewFile();
			}
			Properties p = new Properties();
			p.load(new FileInputStream(f));
			return p;
		} catch (Exception e) {
		}
		return null;
	}
}
