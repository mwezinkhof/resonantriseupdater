/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package novaz;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import novaz.data.json.Pack;
import novaz.tools.Log;
import novaz.tools.LogLevel;
import novaz.worker.ModPackDownloader;
import novaz.worker.ServerManager;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by kaaz on 22-4-2015.
 */
public class Launcher {
	public static void main(String... args) throws IOException, ParserConfigurationException, SAXException, InterruptedException {
		Config.loadProperties();
		ServerManager serverManager = new ServerManager("localhost", Config.getMinecraftServerProperty("server-port", "25565"));
		for (String arg : args) {
			if (arg.contains("=")) {
				String[] override = arg.split("=");
				if (override[0].equalsIgnoreCase("serverdir")) {
					Config.SERVER_DIR = override[1].trim();
				} else if (override[0].equalsIgnoreCase("pack")) {
					Config.PACK_NAME = override[1].trim();
				}
			}
			if (arg.equals("-verbose")) {
				Config.loglevel = LogLevel.DEBUG;
			}
		}

		URL url = new URL(Config.BASE_URL + Config.PACK_LIST);
		String packname = "", packVersion = "";
		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.connect();
		try (Reader reader = new InputStreamReader((InputStream) request.getContent(), "UTF-8")) {
			Gson gson = new GsonBuilder().create();
			Pack[] packList = gson.fromJson(reader, Pack[].class);
			for (Pack p : packList) {
				if (p.name.equalsIgnoreCase(Config.PACK_NAME)) {
					packname = p.name;
					if (!p.versions.isEmpty()) {
						packVersion = p.versions.get(0).version;
					}
					break;
				}
			}
			request.disconnect();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		if (!packname.isEmpty() && !packVersion.isEmpty()) {
			if (!packVersion.equals(Config.INSTALLED_PACK_VERSION)) {
				Log.info("Found a new version!");
				serverManager.stop();
				Thread.sleep(10000);
				Log.info("Downloading new version");
				ModPackDownloader md = new ModPackDownloader(packname, packVersion);
				md.addRequirement("Mainline");
				md.addRequirement("RF Tools");
				if (md.updateServer()) {
					Config.INSTALLED_PACK_VERSION = packVersion;
					Config.saveProperties();
				}
			}
			if (serverManager.isOnline()) {
				if (Config.loglevel == LogLevel.DEBUG) {
					serverManager.sendMessage("Just checking.");
				}
			}
			serverManager.start();
		}
	}
}
