Update/manage tool for Resonant Rise [atlauncher]
==========================================
Downloads the latest [server] version of the resonant rise and outputs it to ./server/ (by default)

Starts a new screen {default name= MC_SERVER} and runs the RR server

The server directory is relative to where the tool was called from

Downloads all required+dependencies+Main Line+RF Tools


STEPS
=====
It does the following things.

* Check for new version, if there isn't: stop
* Stops the MC server
* Backup world, if it failed: stop
* Delete old config/mods/libraries
* Download/unzip new files
* Save some stuff to PackUpdater.cfg
* Start the MC server

PARAMS
======
The default values are set in the data.Config class  
optional parameters:
  java -jar jarfile.jar "serverdir=server/" "pack=Resonant Rise" -verbose